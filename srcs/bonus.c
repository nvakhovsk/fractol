/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bonus.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 15:33:05 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/07 18:05:50 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

void	ft_newton_ri(float **mem, t_fractal *fractal, t_im *in)
{
	float		p;
	t_complex	d;

	mem = NULL;
	fractal->n = 0;
	fractal->z.r = (fractal->nf.r) * 0.005 * in->zoom;
	fractal->z.i = (fractal->nf.i) * 0.005 * in->zoom;
	d = fractal->z;
	while (fractal->z.r * fractal->z.r + fractal->z.i * fractal->z.i
		< fractal->max && d.r * d.r + d.i * d.i < 16
		&& fractal->n < fractal->iter)
	{
		fractal->temp = fractal->z;
		p = (fractal->temp.r * fractal->temp.r +
			fractal->temp.i * fractal->temp.i) * (fractal->temp.r *
			fractal->temp.r + fractal->temp.i * fractal->temp.i);
		fractal->z.r = 2 * fractal->temp.r / 3 +
			(fractal->temp.r * fractal->temp.r - fractal->temp.i *
				fractal->temp.i) / (3 * p);
		fractal->z.i = (2 * fractal->temp.i) * (1 - fractal->temp.r / p) / 3;
		d.r = fabs(fractal->temp.r - fractal->z.r);
		d.i = fabs(fractal->temp.i - fractal->z.i);
		fractal->n++;
	}
}

void	ft_polikarp_ri(float **mem, t_fractal *fractal, t_im *in)
{
	fractal->n = 0;
	(*mem)[0] = 0.11;
	(*mem)[1] = -0.65;
	fractal->z.r = (fractal->nf.r) * 0.005 * in->zoom;
	fractal->z.i = (fractal->nf.i) * 0.005 * in->zoom;
	while (fractal->z.r * fractal->z.r + fractal->z.i * fractal->z.i
		< fractal->max && fractal->n < fractal->iter)
	{
		fractal->temp = fractal->z;
		fractal->z.r = tan((fractal->temp.r * fractal->temp.r) -
			(fractal->temp.i * fractal->temp.i)) + (*mem)[0];
		fractal->z.i = (2 * fractal->temp.r * fractal->temp.i) + (*mem)[1];
		fractal->n++;
	}
}

void	ft_star_ri(float **mem, t_fractal *fractal, t_im *in)
{
	float		p;
	t_complex	d;

	mem = NULL;
	fractal->n = 0;
	fractal->z.r = (fractal->nf.r) * 0.005 * in->zoom;
	fractal->z.i = (fractal->nf.i) * 0.005 * in->zoom;
	d = fractal->z;
	while (fractal->z.r * fractal->z.r + fractal->z.i * fractal->z.i
		< fractal->max && d.r * d.r + d.i * d.i < 16
		&& fractal->n < fractal->iter)
	{
		fractal->temp = fractal->z;
		p = (fractal->temp.r * fractal->temp.r + fractal->temp.i *
			fractal->temp.i) * (fractal->temp.r * fractal->temp.r +
			fractal->temp.i * fractal->temp.i);
		fractal->z.r = 2 * fractal->temp.r / 3 + sin(fractal->temp.r *
			fractal->temp.r - fractal->temp.i * fractal->temp.i) / (3 * p);
		fractal->z.i = (2 * fractal->temp.i) * tan(1 - fractal->temp.r / p) / 3;
		d.r = fabs(fractal->temp.r - fractal->z.r);
		d.i = fabs(fractal->temp.i - fractal->z.i);
		fractal->n++;
	}
}

void	ft_frog_ri(float **mem, t_fractal *fractal, t_im *in)
{
	fractal->n = 0;
	(*mem)[0] = 0.11;
	(*mem)[1] = -0.65;
	fractal->z.r = (fractal->nf.r) * 0.005 * in->zoom;
	fractal->z.i = (fractal->nf.i) * 0.005 * in->zoom;
	while (fractal->z.r * fractal->z.r + fractal->z.i * fractal->z.i
		< fractal->max
		&& fractal->n < fractal->iter)
	{
		fractal->temp = fractal->z;
		fractal->z.r = fabs(fractal->temp.r * fractal->temp.r -
			fractal->temp.i * fractal->temp.i + (*mem)[0]);
		fractal->z.i = (2 * fractal->temp.r * fractal->temp.i) + (*mem)[1];
		fractal->n++;
	}
}
