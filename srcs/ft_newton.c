/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_newton.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 14:34:56 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/07 15:31:20 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
/*
static void	ft_j_norma_dura(t_fra *fra, t_im *mlx)
{
	float	p;
	t_comp	d;

	fra->n = 0;
	fra->z.x = (fra->c.x) * 0.005 * mlx->zoom;
	fra->z.y = (fra->c.y) * 0.005 * mlx->zoom;
	d = fra->z;
	while (fra->z.x * fra->z.x + fra->z.y * fra->z.y < fra->max && d.x * d.x + d.y * d.y < 16
		&& fra->n < fra->iter)
	{
		fra->t = fra->z;
		p = (fra->t.x * fra->t.x + fra->t.y * fra->t.y) * (fra->t.x * fra->t.x + fra->t.y * fra->t.y);
		fra->z.x = 2 * fra->t.x / 3 + (fra->t.x * fra->t.x - fra->t.y * fra->t.y) / (3 * p);
		fra->z.y = (2 * fra->t.y) * (1 - fra->t.x / p) / 3;
		d.x = fabs(fra->t.x - fra->z.x);
		d.y = fabs(fra->t.y - fra->z.y);
		fra->n++;
	}
}

void		ft_newton(t_im *mlx, int f)
{
	t_fra	fra;

	ft_fractal_init(&fra, mlx);
	fra.c.y = -fra.my + mlx->ydelta / mlx->zoom + mlx->yst;
	while (fra.y <= H)
	{
		fra.c.x = -fra.mx + mlx->xdelta / mlx->zoom + mlx->xst;
		fra.x = 0;
		while (fra.x <= W)
		{
			ft_j_norma_dura(&fra, mlx);
			if (fra.n < fra.iter)
			{
								if (f == 0)
					ft_pxl_img(fra.x, fra.y, (mlx->color - fra.n % mlx->color) * (mlx->color -
						fra.n % mlx->color) * (mlx->color - fra.n % mlx->color), mlx);
				else
					ft_pxl_img2(fra.x, fra.y, (mlx->color - fra.n % mlx->color) * (mlx->color -
						fra.n % mlx->color) * (mlx->color - fra.n % mlx->color), mlx);
			}
			fra.x++;
			fra.c.x++;
		}
		fra.y++;
		fra.c.y++;
	}
}
*/