/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 15:08:04 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/07 18:11:29 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ft_exit(void *r)
{
	if (!r)
		exit(0);
	return (0);
}

void	ft_put_pxl(t_im *in, t_fractal fractal, int f)
{
	if (f == 0)
		ft_pxlimg(fractal.x, fractal.y, (in->color - fractal.n % in->color) *
			(in->color - fractal.n % in->color) *
				(in->color - fractal.n % in->color), in);
	else
		ft_pxlimg2(fractal.x, fractal.y, (in->color - fractal.n % in->color) *
			(in->color - fractal.n % in->color) *
				(in->color - fractal.n % in->color), in);
}

void	ft_fractals_init(char ***fra)
{
	(*fra)[0] = "Julia";
	(*fra)[1] = "Mandelbrot";
	(*fra)[2] = "Frog";
	(*fra)[3] = "Newton";
	(*fra)[4] = "Biomorph";
	(*fra)[5] = "Star";
	(*fra)[6] = "Polikarp";
	(*fra)[7] = "Bear";
	(*fra)[8] = NULL;
}

int		ft_check_list(char *str, char ***fr)
{
	int		i;

	i = -1;
	ft_fractals_init(fr);
	while (++i < 8)
		if (ft_strcmp(str, (*fr)[i]) == 0)
			return (1);
	i = -1;
	while (++i < 8)
	{
		ft_putnbr(i);
		ft_putstr(": ");
		ft_putstr((*fr)[i]);
		ft_putendl("");
	}
	exit(0);
	return (0);
}

int		main(int argc, char **argv)
{
	char	**fr;

	fr = (char **)malloc(sizeof(char *) * 9);
	if (argc < 4 && argc > 0)
	{
		if (argc == 2 && ft_check_list(argv[1], &fr))
			ft_image(argv[1], fr);
		else if (argc == 3 && ft_check_list(argv[1], &fr)
					&& ft_check_list(argv[2], &fr))
			ft_image2(argv[1], argv[2], fr);
	}
	free(fr);
	return (0);
}
