/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractal.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 14:36:12 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/07 18:03:54 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

void		ft_fractal_init(t_fractal *fractal, t_im *in)
{
	fractal->iter = 30 + in->i;
	fractal->max = 16;
	fractal->mx = (W / 2);
	fractal->my = (H / 2);
	fractal->y = 0;
}

static void	ft_bear_ri(float **mem, t_fractal *fractal, t_im *in)
{
	float	cx;
	float	cy;

	fractal->n = 0;
	fractal->z.r = (fractal->nf.r) * 0.005 * in->zoom;
	fractal->z.i = (fractal->nf.i) * 0.005 * in->zoom;
	(*mem)[0] = fractal->z.r;
	(*mem)[1] = fractal->z.r;
	while (fractal->z.r * fractal->z.r + fractal->z.i * fractal->z.i
		< fractal->max && fractal->n < fractal->iter)
	{
		cx = (*mem)[0];
		cy = (*mem)[1];
		fractal->temp = fractal->z;
		fractal->z.r = (fractal->temp.r * fractal->temp.r) - (fractal->temp.i *
			fractal->temp.i) + (*mem)[0];
		fractal->z.i = (2 * fractal->temp.r * fractal->temp.i * fractal->temp.r
			* fractal->temp.i) + (*mem)[1];
		(*mem)[0] = cx / 2 + fractal->z.r;
		(*mem)[1] = cy / 2 + fractal->z.r;
		fractal->n++;
	}
}

void		ft_case(float **mem, t_fractal *fractal, t_im *in, int frac)
{
	(frac == 0) ? ft_julia_ri(mem, fractal, in) : 0;
	(frac == 1) ? ft_mandelbrot_ri(mem, fractal, in) : 0;
	(frac == 2) ? ft_frog_ri(mem, fractal, in) : 0;
	(frac == 3) ? ft_newton_ri(mem, fractal, in) : 0;
	(frac == 4) ? ft_bio_ri(mem, fractal, in) : 0;
	(frac == 5) ? ft_star_ri(mem, fractal, in) : 0;
	(frac == 6) ? ft_polikarp_ri(mem, fractal, in) : 0;
	(frac == 7) ? ft_bear_ri(mem, fractal, in) : 0;
}

void		ft_bear(t_im *in, int f, int frac)
{
	t_fractal	fractal;
	float		*mem;

	ft_fractal_init(&fractal, in);
	mem = (float *)malloc(sizeof(float) * 2);
	fractal.nf.i = -fractal.my + in->ydelta / in->zoom + in->yst;
	while (fractal.y <= H)
	{
		fractal.nf.r = -fractal.mx + in->xdelta / in->zoom + in->xst;
		fractal.x = 0;
		while (fractal.x <= W)
		{
			ft_case(&mem, &fractal, in, frac);
			if (fractal.n < fractal.iter)
				ft_put_pxl(in, fractal, f);
			fractal.x++;
			fractal.nf.r++;
		}
		fractal.y++;
		fractal.nf.i++;
	}
	free(mem);
}
