/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/07 13:53:04 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/07 18:01:00 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_start_fract(t_im *in)
{
	(in->fr == 0) ? ft_bear(in, 0, 0) : 0;
	(in->fr == 1) ? ft_bear(in, 0, 1) : 0;
	(in->fr == 2) ? ft_bear(in, 0, 2) : 0;
	(in->fr == 3) ? ft_bear(in, 0, 3) : 0;
	(in->fr == 4) ? ft_bear(in, 0, 4) : 0;
	(in->fr == 5) ? ft_bear(in, 0, 5) : 0;
	(in->fr == 6) ? ft_bear(in, 0, 6) : 0;
	(in->fr == 7) ? ft_bear(in, 0, 7) : 0;
	(in->fr2 == 0) ? ft_bear(in, 1, 0) : 0;
	(in->fr2 == 1) ? ft_bear(in, 1, 1) : 0;
	(in->fr2 == 2) ? ft_bear(in, 1, 2) : 0;
	(in->fr2 == 3) ? ft_bear(in, 1, 3) : 0;
	(in->fr2 == 4) ? ft_bear(in, 1, 4) : 0;
	(in->fr2 == 5) ? ft_bear(in, 1, 5) : 0;
	(in->fr2 == 6) ? ft_bear(in, 1, 6) : 0;
	(in->fr2 == 7) ? ft_bear(in, 1, 7) : 0;
}

void	ft_struct_init(t_im *in, int frac_fl, char **fr)
{
	in->zoom = 1;
	in->xdelta = 0;
	in->ydelta = 0;
	in->color = 30;
	in->ch_jul = 0;
	in->i = 0;
	in->fr = frac_fl;
	in->fr2 = -1;
	in->fra = fr;
	in->ch_flag = -1;
}

void	ft_reimage(t_im *in)
{
	if (in->frac_fl)
	{
		mlx_destroy_image(in->init, in->image2);
		in->image2 = mlx_new_image(in->init, W, H);
	}
	mlx_destroy_image(in->init, in->image);
	in->image = mlx_new_image(in->init, W, H);
	ft_start_fract(in);
	if (in->frac_fl)
	{
		mlx_clear_window(in->init, in->win2);
		mlx_put_image_to_window(in->init, in->win2, in->image2, 0, 0);
	}
	mlx_clear_window(in->init, in->win);
	mlx_put_image_to_window(in->init, in->win, in->image, 0, 0);
}

void	ft_pxlimg(int x, int y, int rgb, t_im *in)
{
	int				bitspp;
	int				slen;
	int				en;
	char			*image;
	unsigned int	tmp;

	image = mlx_get_data_addr(in->image, &bitspp, &slen, &en);
	tmp = (mlx_get_color_value(in->init, rgb));
	if (y > 0 && y < H && x > 0 && x < W)
		ft_memcpy((void *)(image + slen * y + x * sizeof(int)),
			(void *)&tmp, 4);
}

int		ft_mouse_hook(int key, int x, int y, t_im *in)
{
	if (key == 4 || key == 5)
	{
		x -= W / 2;
		y -= H / 2;
		in->xdelta += x * in->zoom;
		in->ydelta += y * in->zoom;
		in->zoom = (key == 4) ? in->zoom / 1.2 : in->zoom * 1.2;
		in->xdelta -= x * in->zoom;
		in->ydelta -= y * in->zoom;
		ft_reimage(in);
	}
	return (0);
}
