/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mandelbrot.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 14:34:56 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/07 15:31:18 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
/*
static void	ft_man_norma_dura(float **cxy, t_fra *fra, t_im *mlx)
{
	fra->n = 0;
	(*cxy)[0] = (fra->c.x) * 0.005 * mlx->zoom;
	(*cxy)[1] = (fra->c.y) * 0.005 * mlx->zoom;
	fra->z.x = 0;
	fra->z.y = 0;
	while (fra->z.x * fra->z.x + fra->z.y * fra->z.y < fra->max
		&& fra->n < fra->iter)
	{
		fra->t = fra->z;
		fra->z.x = (fra->t.x * fra->t.x) - (fra->t.y * fra->t.y)
			+ (*cxy)[0];
		fra->z.y = (2 * fra->t.x * fra->t.y) + (*cxy)[1];
		fra->n++;
	}
}

void		ft_mandelbrot(t_im *mlx, int f)
{
	t_fra	fra;
	float	*cxy;

	ft_fractal_init(&fra, mlx);
	cxy = (float *)malloc(sizeof(float) * 2);
	fra.c.y = -fra.my + mlx->ydelta / mlx->zoom + mlx->yst;
	while (fra.y <= H)
	{
		fra.c.x = -fra.mx + mlx->xdelta / mlx->zoom + mlx->xst;
		fra.x = 0;
		while (fra.x <= W)
		{
			ft_man_norma_dura(&cxy, &fra, mlx);
			if (fra.n < fra.iter)
			{
								if (f == 0)
					ft_pxl_img(fra.x, fra.y, (mlx->color - fra.n % mlx->color) * (mlx->color -
						fra.n % mlx->color) * (mlx->color - fra.n % mlx->color), mlx);
				else
					ft_pxl_img2(fra.x, fra.y, (mlx->color - fra.n % mlx->color) * (mlx->color -
						fra.n % mlx->color) * (mlx->color - fra.n % mlx->color), mlx);
			}
			fra.x++;
			fra.c.x++;
		}
		fra.y++;
		fra.c.y++;
	}
	free(cxy);
}
*/