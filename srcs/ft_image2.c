/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_image2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/07 13:33:20 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/07 18:12:14 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_struct_init2(t_im *in, int frac_fl, int frac_fl2, char **fr)
{
	in->zoom = 1;
	in->xdelta = 0;
	in->ydelta = 0;
	in->color = 30;
	in->ch_jul = 0;
	in->i = 0;
	in->fr = frac_fl;
	in->fr2 = frac_fl2;
	in->fra = fr;
	in->ch_flag = -1;
}

void	ft_pxlimg2(int x, int y, int rgb, t_im *in)
{
	int				bitspp;
	int				slen;
	int				en;
	char			*image;
	unsigned int	tmp;

	image = mlx_get_data_addr(in->image2, &bitspp, &slen, &en);
	tmp = (mlx_get_color_value(in->init, rgb));
	if (x > 0 && x < W && y > 0 && y < H)
	{
		ft_memcpy((void *)(image + slen * y + x * sizeof(int)),
			(void *)&tmp, 4);
	}
}

void	ft_hooks_image2(t_im *in, int frac_fl, int frac_fl2, char **fr)
{
	in->image = mlx_new_image(in->init, W, H);
	in->image2 = mlx_new_image(in->init, W, H);
	ft_struct_init2(in, frac_fl, frac_fl2, fr);
	ft_start_fract(in);
	mlx_put_image_to_window(in->init, in->win, in->image, 0, 0);
	mlx_put_image_to_window(in->init, in->win2, in->image2, 0, 0);
	mlx_mouse_hook(in->win, ft_mouse_hook, in);
	mlx_mouse_hook(in->win2, ft_mouse_hook, in);
	mlx_key_hook(in->win, ft_key_hook, in);
	mlx_key_hook(in->win2, ft_key_hook, in);
	mlx_hook(in->win, 17, 0L, ft_exit, NULL);
	mlx_hook(in->win2, 17, 0L, ft_exit, NULL);
	mlx_loop(in->init);
}

void	ft_image2(char *name, char *name2, char **fr)
{
	int		i;
	int		j;
	int		frac_fl;
	int		frac_fl2;
	t_im	*in;

	i = -1;
	while (++i < 8)
		if (ft_strcmp(name, fr[i]) == 0)
			break ;
	frac_fl = i;
	j = -1;
	while (++j < 8)
		if (ft_strcmp(name2, fr[j]) == 0)
			break ;
	frac_fl2 = j;
	in = (t_im *)malloc(sizeof(t_im));
	in->init = mlx_init();
	in->frac_fl = 1;
	in->win = mlx_new_window(in->init, W, H, fr[i]);
	in->win2 = mlx_new_window(in->init, W, H, fr[j]);
	ft_hooks_image2(in, frac_fl, frac_fl2, fr);
}
