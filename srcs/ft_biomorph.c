/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_biomorph.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 14:34:39 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/07 17:59:23 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_bio_ri(float **mem, t_fractal *fractal, t_im *in)
{
	fractal->n = 0;
	(*mem)[0] = 0.11;
	(*mem)[1] = -0.65;
	fractal->z.r = (fractal->nf.x) * 0.005 * in->zoom;
	fractal->z.i = (fractal->nf.y) * 0.005 * in->zoom;
	while (fractal->z.r * fractal->z.r + fractal->z.i * fractal->z.i
		< fractal->max && fractal->n < fractal->iter)
	{
		fractal->temp = fractal->z;
		fractal->z.r = (fractal->temp.r * fractal->temp.r * fractal->temp.r *
			fractal->temp.r) - (fractal->temp.i * fractal->temp.i *
			fractal->temp.i * fractal->temp.i) - 6 * fractal->temp.i *
			fractal->temp.i * fractal->temp.r * fractal->temp.r + (*mem)[0];
		fractal->z.i = (4 * fractal->temp.r * fractal->temp.r * fractal->temp.r
			* fractal->temp.i) - (4 * fractal->temp.r * fractal->temp.i *
			fractal->temp.i * fractal->temp.i) + (*mem)[1];
		fractal->n++;
	}
}
