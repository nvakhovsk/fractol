/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mjb.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 15:33:05 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/07 18:00:38 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fractol.h>

void	ft_bio_ri(float **mem, t_fractal *fractal, t_im *in)
{
	fractal->n = 0;
	(*mem)[0] = 0.11;
	(*mem)[1] = -0.65;
	fractal->z.r = (fractal->nf.r) * 0.005 * in->zoom;
	fractal->z.i = (fractal->nf.i) * 0.005 * in->zoom;
	while (fractal->z.r * fractal->z.r + fractal->z.i * fractal->z.i
		< fractal->max && fractal->n < fractal->iter)
	{
		fractal->temp = fractal->z;
		fractal->z.r = (fractal->temp.r * fractal->temp.r * fractal->temp.r *
			fractal->temp.r) - (fractal->temp.i * fractal->temp.i *
			fractal->temp.i * fractal->temp.i) - 6 * fractal->temp.i *
			fractal->temp.i * fractal->temp.r * fractal->temp.r + (*mem)[0];
		fractal->z.i = (4 * fractal->temp.r * fractal->temp.r * fractal->temp.r
			* fractal->temp.i) - (4 * fractal->temp.r * fractal->temp.i *
			fractal->temp.i * fractal->temp.i) + (*mem)[1];
		fractal->n++;
	}
}

void	ft_julia_ri(float **mem, t_fractal *fractal, t_im *in)
{
	fractal->n = 0;
	(*mem)[0] = 0.11 + in->ch_jul;
	(*mem)[1] = -0.65 + in->ch_jul;
	fractal->z.r = (fractal->nf.r) * 0.005 * in->zoom;
	fractal->z.i = (fractal->nf.i) * 0.005 * in->zoom;
	while (fractal->z.r * fractal->z.r + fractal->z.i * fractal->z.i
		< fractal->max && fractal->n < fractal->iter)
	{
		fractal->temp = fractal->z;
		fractal->z.r = (fractal->temp.r * fractal->temp.r) -
			(fractal->temp.i * fractal->temp.i) + (*mem)[0];
		fractal->z.i = (2 * fractal->temp.r * fractal->temp.i) + (*mem)[1];
		fractal->n++;
	}
}

void	ft_mandelbrot_ri(float **mem, t_fractal *fractal, t_im *in)
{
	fractal->n = 0;
	(*mem)[0] = (fractal->nf.r) * 0.005 * in->zoom;
	(*mem)[1] = (fractal->nf.i) * 0.005 * in->zoom;
	fractal->z.r = 0;
	fractal->z.i = 0;
	while (fractal->z.r * fractal->z.r + fractal->z.i * fractal->z.i
		< fractal->max && fractal->n < fractal->iter)
	{
		fractal->temp = fractal->z;
		fractal->z.r = (fractal->temp.r * fractal->temp.r) -
			(fractal->temp.i * fractal->temp.i) + (*mem)[0];
		fractal->z.i = (2 * fractal->temp.r * fractal->temp.i) + (*mem)[1];
		fractal->n++;
	}
}
