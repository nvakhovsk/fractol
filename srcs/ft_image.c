/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_image.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/27 13:52:45 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/07 18:10:05 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_put_fl(t_im *in)
{
	if (in->frac_fl)
	{
		mlx_clear_window(in->init, in->win2);
		mlx_put_image_to_window(in->init, in->win2, in->image2, 0, 0);
	}
	mlx_clear_window(in->init, in->win);
	mlx_put_image_to_window(in->init, in->win, in->image, 0, 0);
}

int		ft_ch_jul(int x, int y, t_im *in)
{
	static int flag;

	if (!flag)
		flag = 1;
	if (in->flafj == 1 && x && y && in->fr == 0)
	{
		mlx_destroy_image(in->init, in->image);
		in->image = mlx_new_image(in->init, W, H);
		if (flag == 1)
		{
			in->ch_jul += 0.03;
			if (in->ch_jul > 0.4)
				flag = 2;
		}
		if (flag == 2)
		{
			in->ch_jul -= 0.03;
			if (in->ch_jul < -0.7)
				flag = 1;
		}
		ft_reimage(in);
		ft_put_fl(in);
	}
	return (1);
}

void	ft_ch_frac(int key, t_im *in)
{
	key == 18 ? in->fr = 0 : 0;
	key == 19 ? in->fr = 1 : 0;
	key == 20 ? in->fr = 2 : 0;
	key == 21 ? in->fr = 3 : 0;
	key == 23 ? in->fr = 4 : 0;
	key == 22 ? in->fr = 5 : 0;
	key == 26 ? in->fr = 6 : 0;
	key == 28 ? in->fr = 7 : 0;
	ft_start_fract(in);
}

int		ft_key_hook(int key, t_im *in)
{
	key == 53 ? ft_exit(NULL) : 0;
	key == 8 ? in->color += 30 : 0;
	key == 126 ? in->yst += 5 : 0;
	key == 125 ? in->yst -= 5 : 0;
	key == 124 ? in->xst -= 5 : 0;
	key == 123 ? in->xst += 5 : 0;
	if (key >= 18 && key < 29)
		ft_ch_frac(key, in);
	if (key == 12)
	{
		in->ch_flag += 1;
		if ((in->ch_flag % 2))
		{
			in->flafj = (in->flafj == 0) ? 1 : 0;
			mlx_hook(in->win, 6, 1L << 6, ft_ch_jul, in);
		}
	}
	(key == 69) ? in->i++ : 0;
	key == 78 && in->i >= 0 ? in->i-- : 0;
	ft_reimage(in);
	return (0);
}

void	ft_image(char *name, char **fr)
{
	int		i;
	int		frac_fl;
	t_im	*in;

	i = -1;
	while (++i < 8)
		if (ft_strcmp(name, fr[i]) == 0)
			break ;
	frac_fl = i;
	in = (t_im *)malloc(sizeof(t_im));
	in->init = mlx_init();
	in->frac_fl = 0;
	in->win = mlx_new_window(in->init, W, H, fr[i]);
	in->image = mlx_new_image(in->init, W, H);
	ft_struct_init(in, frac_fl, fr);
	ft_start_fract(in);
	mlx_put_image_to_window(in->init, in->win, in->image, 0, 0);
	mlx_mouse_hook(in->win, ft_mouse_hook, in);
	mlx_key_hook(in->win, ft_key_hook, in);
	mlx_hook(in->win, 17, 0L, ft_exit, NULL);
	mlx_loop(in->init);
}
