# **Fract'ol** #
 
## А fractal renderer.
## **Features**
* Render different fractals: mandelbrot, julia, newton and others. 
* Different color schemes
## **Compiling and running** 
* Run make. An executable will compile. Runs on OS X and Linux.
* Run it with ./fractol [fractal]. 
## Possible fractals are: Mandelbrot, Julia, Frog, Newton, Biomorph, Star, Polikarp, Bear.
## **Shortcuts** ##
*  Increase/decrease maximum iterations with + and - on the numpad. 
*  Zoom in and out with the mousewheel.
*  Close the program	 esc 


![Screen Shot 2017-04-29 at 9.44.00 AM.png](https://bitbucket.org/repo/LoRjak5/images/4235968685-Screen%20Shot%202017-04-29%20at%209.44.00%20AM.png)
![Screen Shot 2017-04-29 at 9.49.03 AM.png](https://bitbucket.org/repo/LoRjak5/images/3462876319-Screen%20Shot%202017-04-29%20at%209.49.03%20AM.png)
![Screen Shot 2017-04-29 at 9.50.30 AM.png](https://bitbucket.org/repo/LoRjak5/images/1259829678-Screen%20Shot%202017-04-29%20at%209.50.30%20AM.png)
![Screen Shot 2017-04-29 at 9.52.40 AM.png](https://bitbucket.org/repo/LoRjak5/images/247127364-Screen%20Shot%202017-04-29%20at%209.52.40%20AM.png)
![Screen Shot 2017-04-29 at 9.54.37 AM.png](https://bitbucket.org/repo/LoRjak5/images/3186273558-Screen%20Shot%202017-04-29%20at%209.54.37%20AM.png)
![Screen Shot 2017-04-29 at 9.57.43 AM.png](https://bitbucket.org/repo/LoRjak5/images/1512178030-Screen%20Shot%202017-04-29%20at%209.57.43%20AM.png)
![Screen Shot 2017-04-29 at 9.55.02 AM.png](https://bitbucket.org/repo/LoRjak5/images/923509008-Screen%20Shot%202017-04-29%20at%209.55.02%20AM.png)

