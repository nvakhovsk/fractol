# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/20 09:53:11 by nmatushe          #+#    #+#              #
#    Updated: 2017/04/07 15:38:17 by nmatushe         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol

HEAD = includes/

VPATH = srcs:includes

FLAGS = -O3 -Wall -Wextra -Werror -I $(HEAD)

MLX = -lmlx -framework AppKit -framework OpenGl

SRCS = main.c									\
		ft_image.c 								\
		tools.c 								\
		ft_image2.c 							\
		ft_fractal.c 							\
		ft_mjb.c 								\
		bonus.c 								\


BINS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(BINS)
	 make re -C libft/ fclean && make -C libft/
	gcc -o $(NAME) -I libft/includes $(BINS) $(FLAGS) -L. libft/libft.a $(MLX)

%.o: %.c
	gcc -I libft/includes $(FLAGS) -c -o $@ $<

clean:
	/bin/rm -f $(BINS) && make -C libft/ fclean

fclean: clean
	/bin/rm -f $(NAME)

re: fclean all
