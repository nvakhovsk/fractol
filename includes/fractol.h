/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 12:21:29 by nmatushe          #+#    #+#             */
/*   Updated: 2017/04/07 18:23:28 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <fcntl.h>
# include <stdio.h>
# include "libft.h"
# include <mlx.h>
# include <math.h>
# define W 1000
# define H 1000
# define COF	0.31

typedef struct		s_im
{
	void	*init;
	void	*win;
	void	*win2;
	void	*image;
	void	*image2;
	float	zoom;
	float	xdelta;
	float	ydelta;
	int		fr;
	int		fr2;
	int		color;
	float	ch_jul;
	int		i;
	int		ch_flag;
	int		flafj;
	int		xst;
	int		yst;
	char	**fra;
	int		frac_fl;
}					t_im;

typedef struct		s_complex
{
	double	r;
	double	i;
}					t_complex;

typedef struct		s_fractal
{
	int			x;
	int			y;
	int			n;
	int			mx;
	int			my;
	int			iter;
	int			max;
	t_complex	z;
	t_complex	nf;
	t_complex	temp;
}					t_fractal;

typedef struct		s_brezen
{
	float		dx;
	float		dy;
	float		sx;
	float		sy;
	float		err0;
	float		err1;
}					t_brezen;

void				ft_image(char *name, char **fr);
void				ft_image2(char *name, char *name2, char **fr);
int					ft_mouse_hook(int key, int x, int y, t_im *in);
int					ft_exit(void *a);
void				ft_start_fract(t_im *in);
void				ft_struct_init(t_im *in, int frac_fl, char **fr);
void				ft_reimage(t_im *in);
int					ft_mouse_hook(int key, int x, int y, t_im *in);
void				ft_ch_frac(int key, t_im *in);
int					ft_key_hook(int key, t_im *in);
void				ft_put_pxl(t_im *in, t_fractal fractal, int f);
int					ft_ch_jul(int x, int y, t_im *in);
void				ft_bear(t_im *in, int f, int frac);
void				ft_frog_ri(float **mem, t_fractal *fractal, t_im *in);
void				ft_julia_ri(float **mem, t_fractal *fractal, t_im *in);
void				ft_bio_ri(float **mem, t_fractal *fractal, t_im *in);
void				ft_mandelbrot_ri(float **mem, t_fractal *fractal, t_im *in);
void				ft_newton_ri(float **mem, t_fractal *fractal, t_im *in);
void				ft_polikarp_ri(float **mem, t_fractal *fractal, t_im *in);
void				ft_star_ri(float **mem, t_fractal *fractal, t_im *in);
void				ft_fra_init(t_fractal *fra, t_im *in);
void				ft_pxlimg(int x, int y, int rgb, t_im *in);
void				ft_pxlimg2(int x, int y, int rgb, t_im *in);

#endif
