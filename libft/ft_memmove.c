/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 08:08:31 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/28 16:03:36 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	unsigned long	i;
	unsigned long	l;
	unsigned char	*d;
	unsigned char	*s;

	i = 0;
	l = (unsigned long)n;
	d = (unsigned char *)dest;
	s = (unsigned char *)src;
	if (s == d)
		return (d);
	if (s > d)
	{
		while (++i <= l)
			d[i - 1] = s[i - 1];
	}
	else
	{
		while (++i <= l)
			d[l - i] = s[l - i];
	}
	return (d);
}
