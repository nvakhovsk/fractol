/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 08:08:31 by nmatushe          #+#    #+#             */
/*   Updated: 2017/01/18 10:47:33 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	unsigned int	l1;
	char			*str;

	if ((!(s1)) || (!(s2)))
		return (0);
	l1 = ft_strlen(s1);
	str = ft_strnew(l1 + ft_strlen(s2));
	if (!str)
		return (NULL);
	str = ft_strcat(str, s1);
	ft_strcat(str + l1, s2);
	return (str);
}
