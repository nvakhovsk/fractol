/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 08:08:31 by nmatushe          #+#    #+#             */
/*   Updated: 2016/11/28 16:11:21 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int		ft_memcmp(const void *str1, const void *str2, size_t n)
{
	unsigned char	*dest1;
	unsigned char	*dest2;

	dest1 = (unsigned char*)str1;
	dest2 = (unsigned char*)str2;
	while (n--)
	{
		if (*dest1 - *dest2)
			return ((int)(*dest1 - *dest2));
		else
		{
			dest1++;
			dest2++;
		}
	}
	return (0);
}
