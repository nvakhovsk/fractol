/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 14:22:59 by nmatushe          #+#    #+#             */
/*   Updated: 2016/12/01 16:17:22 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void			ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*enext;
	t_list	*ecur;

	ecur = *alst;
	enext = ecur->next;
	del(ecur->content, ecur->content_size);
	ecur = enext;
	while (ecur)
	{
		enext = ecur->next;
		del(ecur->content, ecur->content_size);
		free(ecur);
		ecur = enext;
	}
	free(*alst);
	*alst = 0;
}
